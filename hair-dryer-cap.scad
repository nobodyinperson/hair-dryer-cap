// clang-format off
include <utils/utils.scad>;
// clang-format on

/* [⚫ Cap] */
cap_inner_diameter = 68;
cap_thickness = 2.5;
cap_depth = 11;
cap_hole_size = 4;
cap_hole_distance = 5;

// Precision
epsilon = 0.1 * 1;
$fa = $preview ? 2 : 0.5;
$fs = $preview ? 1 : 0.5;

module
siv()
{
  offset_fun = function(i, j, d)[j % 2 == 0 ? 0 : d / 2, 0];
  position_fun = function(i, j, d)[i * d, sin(60) * j * d];
  n = floor(cap_inner_diameter / cap_hole_distance);
  mosaic(ix = [-n:n],
         iy = [-n:n],
         distance = cap_hole_distance,
         position = position_fun,
         offset = offset_fun,
         decider = function(i, j, d)(
           norm(position_fun(i, j, d) + offset_fun(i, j, d)) <
           cap_inner_diameter / 2 - d / 2)) circle(d = cap_hole_size);
}

// attachment rim
difference()
{
  linear_extrude(cap_depth + cap_thickness) difference()
  {
    circle(d = cap_inner_diameter + 2 * cap_thickness);
    circle(d = cap_inner_diameter);
  }
}

// ventilation holes
linear_extrude(cap_thickness) difference()
{
  circle(d = cap_inner_diameter + 2 * cap_thickness);
  siv();
}

